import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import Election from "./Election";

class Tables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      candidates1: [],
      candidates2: []
    }
  }

  async componentDidMount() {
    const owner = await Election.methods.owner().call();
    const candidates1 = await Election.methods.candidates("0").call();
    const candidates2 = await Election.methods.candidates("1").call();
    const totalVotes = await Election.methods.totalVotes().call();
    this.setState({ owner, candidates1, candidates2, totalVotes });
  }

  onClick = async () => {
    const response = await Election.methods.getArray().call();
    const responser = response.toString().replace(/0x/g, '\n0x');
    this.setState({ message: responser });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="8">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> List
              </CardHeader>
              <CardBody>
                <Col md="10">
                  These are the candidates :
                </Col>
                <Col md="10">
                  0: <strong>{this.state.candidates1.name}</strong>  {this.state.candidates1.count}{" "}
                </Col>
                <Col md="10">
                  1: <strong>{this.state.candidates2.name}</strong>
                </Col>
                <Col md="10">
                  You can Choose your according to your candidates name.
                </Col>
                
                <Table responsive>
                  <thead>
                    <tr>
                      <th>Hashcode</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr width='70%'>
                      <p>{this.state.message}</p>
                    </tr>
                    <tr>
                      <button type="button" onClick={this.onClick}>Show!</button>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Tables;