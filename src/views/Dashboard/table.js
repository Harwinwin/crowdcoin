import React, {Component} from "react";
import MUIDataTable from "mui-datatables";
import {withRouter } from 'react-router-dom';
import { createMuiTheme, MuiThemeProvider, withStyles } from "@material-ui/core/styles"

class TableCamp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Data: null
    };
  }

//   componentDidMount() {
//     Api.getDemographictableIG()
//       .then((res) => {

//         this.setState({
//           Data: res.rows
//         });
//         console.log('res', this.state.Data)
//       })
//       .catch((err) => {
//         this.setState({
//           Data: err
//         });
//       })
//   }

  render() {
    const columns = ["Report Date", "Men", "Women", "13-17", "18-24", "25-34", "35+"];

    // const data = [
    //  ["27-11-2019", "50%", "50%", "91", "235", "747", "37"],
    // ];


    const options = {
      filterType: 'checkbox',
      download: false,
      print: false
    };

    return (
      <div>

        {this.state.Data ? (
          <MUIDataTable
            title={"Demographic"}
            // data={this.state.Data.map(d => [
            //   `${d.date}`, `${d.men}`,`${d.women}`, `${d.age1}`, `${d.age2}`, `${d.age3}`, `${d.age4}`
            // ])}
            columns={columns}
            options={options}
          />
        ) : (
            <div></div>
          )}
      </div>
    );
  }
}

export default withRouter(withStyles(TableCamp))